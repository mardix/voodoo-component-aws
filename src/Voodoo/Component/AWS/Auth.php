<?php

/**
 * Auth
 * 
 * It gets the credentials
 * 
 * @name Auth
 * @author Mardix
 */
namespace Voodoo\Component\AWS;

use Voodoo;

class Auth {

    /**
     * Get the access key
     * @return string
     */
    public static function getAccessKey()
    {
        return Voodoo\Core\Config::Component()->get("AWS.accessKey");
    }

    /**
     * Get the access key
     * @return string
     */    
    public static function getSecretKey()
    {
        return Voodoo\Core\Config::Component()->get("AWS.secretKey");
    }    
    
    /**
     * Get the access key
     * @return string
     */    
    public static function useSSL()
    {
        return Voodoo\Core\Config::Component()->get("AWS.useSSL");
    }    
    
    /**
     * Get the access key
     * @return string
     */    
    public static function getEndPoint()
    {
        return Voodoo\Core\Config::Component()->get("AWS.enpoint");
    }   
    
    /**
     * Get the access key
     * @return string
     */    
    public static function getAccountId()
    {
        return Voodoo\Core\Config::Component()->get("AWS.accountId");
    }     
    
    /**
     * Get the access key
     * @return string
     */    
    public static function getS3BucketName()
    {
        return Voodoo\Core\Config::Component()->get("AWS.S3_BucketName");
    }     
}
