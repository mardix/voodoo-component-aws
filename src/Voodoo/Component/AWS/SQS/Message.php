<?php

/**
 * Message
 * 
 * @name Message
 * @author Mardix
 */
namespace Voodoo\Component\AWS\SQS;

use Voodoo\Component\AWS\SQS as SQSClient;

class Message 
{
    private $sqs, $data;
    
    public function __construct(SQSClient $sqs, $m) {
        $this->sqs = $sqs;
        $message = [];
        $message['MessageId'] = (string)(@$m->MessageId);
        $message['ReceiptHandle'] = (string)(@$m->ReceiptHandle);
        $message['MD5OfBody'] = (string)(@$m->MD5OfBody);
        $message['Body'] = (string)(@$m->Body);

        if(isset($m->Attribute)) {
            $attributes = [];
            foreach($m->Attribute as $a) {
                $attributes[(string)$a->Name] = (string)$a->Value;
            }
            $message['Attributes'] = $attributes;
        } 
        $this->data = $message;
    }
    
    public function getRequestId()
    {
        return $this->requestId;
    }
    
    /**
     * Get body
     * @return string
     */    
    public function getBody()
    {
        return $this->data["Body"];
    }
    
    /**
     * Get message
     * @return string
     */    
    public function getMessageId()
    {
        return $this->data["MessageId"];
    }
    
    /**
     * Get receipt handle
     * @return string
     */    
    public function getReceiptHandle()
    {
        return $this->data["ReceiptHandle"];        
    }
    
    /**
     * Get the MD5 of body
     * @return string
     */
    public function getMD5OfBody()
    {
        return $this->data["MD5OfBody"];        
    }
    
    /**
     * Get the attributes
     * @return Array
     */
    public function getAttributes()
    {
        return $this->data["Attributes"];        
    }
    
    /**
     * To delete message in q
     * @return string
     */
    public function delete()
    {
        $request = new Request($this->sqs, $this->sqs->getQueue(), 'DeleteMessage', 'POST');
        $request->setParameter('ReceiptHandle', $this->getReceiptHandle());
        $request->getResponse();
        return true;
    }
    
    public function changeVisibility($visibility_timeout)
    {
        $request = new Request($this->sqs, $this->sqs->getQueue(), 'ChangeMessageVisibility', 'POST');
        $request->setParameter('ReceiptHandle', $this->getReceiptHandle());
        $request->setParameter('VisibilityTimeout', $visibility_timeout);
        $request->getResponse();
        return $this;       
    }
}
