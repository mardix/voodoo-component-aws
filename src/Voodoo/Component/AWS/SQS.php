<?php
/**
* Copyright (c) 2011, Dan Myers.
* Parts copyright (c) 2008, Donovan Schönknecht.
* All rights reserved.

* Amazon SQS PHP class
*
* @link http://sourceforge.net/projects/php-sqs/
* @version 1.0.0
*/

namespace Voodoo\Component\AWS;

use Exception,
    ArrayIterator;

class SQS
{
	const ENDPOINT_US_EAST = 'https://sqs.us-east-1.amazonaws.com/';
	const ENDPOINT_US_WEST = 'https://sqs.us-west-1.amazonaws.com/';
	const ENDPOINT_EU_WEST = 'https://sqs.eu-west-1.amazonaws.com/';
	const ENDPOINT_AP_SOUTHEAST = 'https://sqs.ap-southeast-1.amazonaws.com/';

	const INSECURE_ENDPOINT_US_EAST = 'http://sqs.us-east-1.amazonaws.com/';
	const INSECURE_ENDPOINT_US_WEST = 'http://sqs.us-west-1.amazonaws.com/';
	const INSECURE_ENDPOINT_EU_WEST = 'http://sqs.eu-west-1.amazonaws.com/';
	const INSECURE_ENDPOINT_AP_SOUTHEAST = 'http://sqs.ap-southeast-1.amazonaws.com/';
	
	protected $accessKey; // AWS Access key
	protected $secretKey; // AWS Secret key
	protected $host;
        protected $queue = "";
        protected $queueName = "";

	public function getAccessKey() { return $this->accessKey; }
	public function getSecretKey() { return $this->secretKey; }
	public function getHost() { return $this->host; }

	protected $__verifyHost = false;
	protected $__verifyPeer = false;

	// verifyHost and verifyPeer determine whether curl verifies ssl certificates.
	// It may be necessary to disable these checks on certain systems.
	public function verifyHost() { return $this->__verifyHost; }
	public function enableVerifyHost($enable = true) { $this->__verifyHost = $enable; }

	public function verifyPeer() { return $this->__verifyPeer; }
	public function enableVerifyPeer($enable = true) { $this->__verifyPeer = $enable; }


        /**
         * Constructor
         * @param string $queueName
         * @param string $host
         */
	public function __construct($queueName = null, $host = SQS::ENDPOINT_US_EAST) {
            $this->host = $host;
            
            if ($queueName) {
               $this->setQueueName($queueName); 
                $this->setQueue($this->host.(Auth::getAccountId())."/$queueName/");
            }
            $this->setAuth(Auth::getAccessKey(), Auth::getSecretKey());
	}

	/**
	* Set AWS access key and secret key
	*
	* @param string $accessKey Access key
	* @param string $secretKey Secret key
	* @return AWS\SQS
	*/
	public function setAuth($accessKey, $secretKey) {
            $this->accessKey = $accessKey;
            $this->secretKey = $secretKey;
            return $this;
	}

        /**
         * Set the name of the Queue
         * @param type $queueName
         * @return AWS\SQS
         */
        public function setQueueName($queueName)
        {
            $this->queueName = $queueName;
            return $this;
        }
        
        /**
         * Set the queue
         * @param type $queue
         * @return AWS\SQS
         */
        public function setQueue($queue)
        {
            $this->queue = $queue;
            return $this;
        }
        
        /**
         * Return the queue
         * @return string
         */
        public function getQueue()
        {
            return $this->queue;
        }
        
        /**
         * Get the queueu url
         * @param string $queue
         * @return string
         */
        public function getQueueUrl($queueName)
        {
            $request = new SQS\Request($this, $this->host, 'GetQueueUrl', 'POST');
            $response = $request->setParameter('QueueName', $queueName)->getResponse();
            return (string)$response->body->GetQueueUrlResult->QueueUrl;          
        }
        
        
	/**
	* Create a queue
	*
	* @param string  $queue The queue to create
	* @param integer $visibility_timeout The visibility timeout for the new queue
	* @return AWS\SQS
	*/
	public function createQueue($queue, $visibility_timeout = null) {
            $request = new SQS\Request($this, $this->host, 'CreateQueue', 'POST');
            $request->setParameter('QueueName', $queue);
            if ($visibility_timeout !== null) {
                $request->setParameter('DefaultVisibilityTimeout', $visibility_timeout);
            }
            $body = $request->getResponse()->body;
            return (new self)->setQueue($body->CreateQueueResult->QueueUrl);
	}

	/**
	* Delete a queue
         * 
	* @return bool
	*/
	public function deleteQueue() {
            $request = new SQS\Request($this, $this->queue, 'DeleteQueue', 'POST');
            $request->getResponse();
            $this->queue = null;
            return true;
	}

	/**
	* Get a list of queues
	*
	* @param string $prefix Only return queues starting with this string (optional)
	* @return Array of AWS\SQS
	*/
	public function listQueues($prefix = null) {
            $request = new SQS\Request($this, $this->host, 'ListQueues', 'GET');
            if($prefix) {
                $request->setParameter('QueueNamePrefix', $prefix);
            }
            $response = $request->getResponse();
            $queues = [];
            if(isset($response->body->ListQueuesResult)) {
                foreach($response->body->ListQueuesResult->QueueUrl as $q) {
                        $queues[] = (new self)->setQueue($q);
                }
            }
            return new ArrayIterator($queues);
	}

	/**
	* Get a queue's attributes
	*
	* @param string $attribute Which attribute to retrieve (default is 'All')
	* @return Array of attributes
	*/
	public function getQueueAttributes($attribute = 'All') {
            $request = new SQS\Request($this, $this->queue, 'GetQueueAttributes', 'GET');
            $request->setParameter('AttributeName', $attribute);
            $response = $request->getResponse();
            $attributes = [];
            if(isset($response->body->GetQueueAttributesResult)) {
                foreach($response->body->GetQueueAttributesResult->Attribute as $a) {
                    $attributes[(string)$a->Name] = (string)$a->Value;
                }
            }
            return $attributes;
	}

	/**
	* Set attributes on a queue
         * 
	* @param Array $attributes An array of name=>value attribute pairs
	* @return AWS\SQS
	*/
	public function setQueueAttributes(Array $attributes) {
            $request = new SQS\Request($this, $this->queue, 'SetQueueAttributes', 'POST');
            $i = 1;
            foreach($attributes as $attribute => $value) {
                $request->setParameter('Attribute.'.$i.'.Name', $attribute);
                $request->setParameter('Attribute.'.$i.'.Value', $value);
                $i++;
            }
            $request->getResponse();
            return $this;
	}

	/**
	* Send a message to a queue
	*
	* @param string $message The body of the message to send
	* @return AWS\SQS\Message
	*/
	public function sendMessage($message) {
            $request = new SQS\Request($this, $this->queue, 'SendMessage', 'POST');
            $request->setParameter('MessageBody', $message);
            $response = $request->getResponse();
            if(isset($response->body->SendMessageResult)) {
                $response->body->SendMessageResult->Body = $message;
            }
            return new SQS\Message($this, $response->body->SendMessageResult);
	}

	/**
	* Receive a message from a queue
	*
	* @param string  $queue The queue for which to retrieve messages
	* @param integer $num_messages The maximum number of messages to retrieve (optional)
	* @param integer $visibility_timeout The visibility timeout of the retrieved message (optional)
	* @param array   $attributes An array of attributes for each message that you want to retrieve (optional)
	* @return ArrayIterator SQS\Message
	*/
	public function receiveMessage($num_messages = null, $visibility_timeout = null, $attributes = array()) {
            $request = new SQS\Request($this, $this->queue, 'ReceiveMessage', 'GET');
            if ($num_messages) {
                $request->setParameter('MaxNumberOfMessages', $num_messages);
            }
            if ($visibility_timeout) {
                $request->setParameter('VisibilityTimeout', $visibility_timeout);
            }

            $i = 1;
            foreach($attributes as $attribute) {
                $request->setParameter('AttributeName.'.$i, $attribute);
                $i++;
            }
            $response = $request->getResponse();

            $messages = [];
            if(isset($response->body->ReceiveMessageResult)) {
                foreach($response->body->ReceiveMessageResult->Message as $m) {
                    $messages[] = new SQS\Message($this, $m);
                }
            }
            return new ArrayIterator($messages);
	}



	/**
	* Add access permissions to a queue, for sharing access to queues with other users
	*
	* @param string $queue The queue to which the permission will be added
	* @param string $label A unique identifier for the new permission
	* @param array  $permissions An array of account id => action name
	* @return AWS\SQS
	*/
	public function addPermission($label, Array $permissions = []) {
            $request = new SQS\Request($this, $this->queue, 'AddPermission', 'POST');
            $request->setParameter('Label', $label);
            
            $i = 1;
            foreach($permissions as $account => $action) {
                $request->setParameter('AWSAccountId.'.$i, $account);
                $request->setParameter('ActionName.'.$i, $action);
                $i++;
            }
            $request->getResponse();
            return $this;
	}

	/**
	* Remove a permission from a queue
         * 
	* @param string $label A unique identifier for the new permission
	* @return AWS\SQS
	*/
	public function removePermission($label) {
            $request = new SQS\Request($this, $this->queue, 'RemovePermission', 'POST');
            $request->setParameter('Label', $label)->getResponse();
            return $this;
	}
}