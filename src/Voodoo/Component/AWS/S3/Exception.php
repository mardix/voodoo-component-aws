<?php

/**
 * Exception
 * 
 * @name Exception
 * @author Mardix
 */
namespace Lib\Component\AWS\S3;

class Exception extends \Exception{
    
    public function __construct($message, $file, $line, $code = 0){
            parent::__construct($message, $code);
            $this->file = $file;
            $this->line = $line;
    }    
}
