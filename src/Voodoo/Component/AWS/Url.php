<?php


namespace Voodoo\Component\AWS;

use Voodoo;

class Url {

    CONST AWS_URL = "http://{{bucketname}}.s3.amazonaws.com";

    private static $bucket = null;
    private static $bucketUrl = null;

    /**
     * Return the bucket name
     *
     * @return string
     */
    public static function getBucketUrl()
    {
        if (! self::$bucket) {
            self::$bucket = $bucket = Voodoo\Core\Config::Component()->get("AWS.S3_BucketName");
            self::$bucketUrl = str_replace("{{bucketname}}", self::$bucket, self::AWS_URL);
        }
        return self::$bucketUrl;
    }


    /**
     * Get the object url
     *
     * @param string $object
     * @return string
     */
    public static function getObjectUrl($object = null)
    {
       if($object) {
           $object = preg_replace("/^\//", "", $object);
       } else {
           $object = "";
       }
       return self::getBucketUrl()."/$object";
    }

    /**
     * Return the bucket name
     *
     * @return string
     */
    public static function getBucketName()
    {
        self::getBucketUrl();
        return self::$bucket;
    }
}

