<?php

/**
 * Message
 *
 * @package     SN
 * @subpackage
 * @author      Mardix <mmacxis@sportingnews.com>
 * @copyright   2013 Sporting News
 */

namespace Voodoo\Component\AWS\SES;

use Voodoo\Component\AWS,
    Exception;

class Message {
    CONST MAX_RECIPIENTS = 50;
    CONST CHARSET = "UTF-8";

    private $to = [], $cc = [], $bcc = [], $replyto = [];
    private $from, $returnpath;
    private $subject, $bodyText, $bodyHtml, $bodyCharset;


    /**
     * To receivers
     * @param mixed $to (string | Array)
     * @return \Lib\Component\AWS\SES\Message
     */
    function to($to) {
        if(!is_array($to)) {
            $this->to[] = $to;
        } else {
            $this->to = array_merge($this->to, $to);
        }
        return $this;
    }

    /**
     * CC receivers
     * @param mixed $cc (string | Array)
     * @return \Lib\Component\AWS\SES\Message
     */
    function cc($cc) {
        if(!is_array($cc)) {
            $this->cc[] = $cc;
        } else {
            $this->cc = array_merge($this->cc, $cc);
        }
        return $this;
    }

    /**
     * bcc receivers
     * @param mixed $bcc (string | Array)
     * @return \Lib\Component\AWS\SES\Message
     */
    function bcc($bcc) {
        if(!is_array($bcc)) {
            $this->bcc[] = $bcc;
        } else {
            $this->bcc = array_merge($this->bcc, $bcc);
        }
        return $this;
    }

    /**
     * reply to
     * @param mixed $replyto (string | Array)
     * @return \Lib\Component\AWS\SES\Message
     */
    function replyTo($replyto) {
        if(!is_array($replyto)) {
            $this->replyto[] = $replyto;
        } else {
            $this->replyto = array_merge($this->replyto, $replyto);
        }
        return $this;
    }

    /**
     * From sender
     * @param string $from - email
     * @param string $name - name
     * @return \Lib\Component\AWS\SES\Message
     */
    function from($from, $name = "") {
        $this->from = ($name) ? "{$name} <{$from}>" : $from;
        return $this;
    }

    /**
     * Return path
     * @param type $returnpath
     * @return \Lib\Component\AWS\SES\Message
     */
    function returnPath($returnpath) {
        $this->returnpath = $returnpath;
        return $this;
    }

    /**
     * Subject
     *
     * @param type $subject
     * @param type $charset
     * @return \Lib\Component\AWS\SES\Message
     */
    function subject($subject, $charset = self::CHARSET) {
        $this->subject = $subject;
        $this->subjectCharset = $charset;
        return $this;
    }

    /**
     * Set the message body
     *
     * @param string $text
     * @param string $html
     * @param string $charset
     * @return \Lib\Component\AWS\SES\Message
     */
    function body($text, $html = null, $charset = self::CHARSET) {
        $this->bodyText = $text;
        $this->bodyHtml = $html;
        $this->bodyCharset = $charset;
        return $this;
    }


    /**
    * Validates whether the message object has sufficient information to submit a request to SES.
    * This does not guarantee the message will arrive, nor that the request will succeed;
    * instead, it makes sure that no required fields are missing.
    *
    * This is used internally before attempting a SendEmail or SendRawEmail request,
    * but it can be used outside of this file if verification is desired.
    * May be useful if e.g. the data is being populated from a form; developers can generally
    * use this function to verify completeness instead of writing custom logic.
    *
    * @return boolean
    */
    public function validate() {
        if ($this->countRecipients() > self::MAX_RECIPIENTS) {
            throw new Exception("Recipients exceed ".self::MAX_RECIPIENTS." max count");
        }
        if (count($this->to) == 0) {
            throw new Exception("No TO destination");
        }
        if (! $this->from) {
            throw new Exception("No FROM destination");
        }
        if (! $this->subject) {
            throw new Exception("No SUBJECT message");
        }
        if (! $this->bodyText && ! $this->bodyHtml) {
            throw new Exception("No MESSAGE content");
        }
        return true;
    }


    /**
     * Return the total recipients
     * @return int
     */
    public function countRecipients()
    {
        return array_sum([count($this->to), count($this->cc), count($this->bcc)]) ;
    }


    /**
     * Prepare the message to be sent
     *
     * @param \Lib\Component\AWS\SES $rest
     * @return \Lib\Component\AWS\SES
     */
    public function prepareMessage(AWS\SES\Request $rest)
    {
        $this->validate();

        $rest->setParameter('Source', $this->from);

        if($this->returnpath) {
            $rest->setParameter('ReturnPath', $this->returnpath);
        }

        if($this->subject != null && strlen($this->subject) > 0) {
            $rest->setParameter('Message.Subject.Data', $this->subject);
            $rest->setParameter('Message.Subject.Charset', $this->subjectCharset);
        }

        $i = 1;
        foreach($this->to as $to) {
            $rest->setParameter('Destination.ToAddresses.member.'.$i++, $to);
        }
        $i = 1;
        foreach($this->cc as $cc) {
            $rest->setParameter('Destination.CcAddresses.member.'.$i++, $cc);
        }
        $i = 1;
        foreach($this->bcc as $bcc) {
            $rest->setParameter('Destination.BccAddresses.member.'.$i++, $bcc);
        }
        $i = 1;
        foreach($this->replyto as $replyto) {
            $rest->setParameter('ReplyToAddresses.member.'.$i++, $replyto);
        }

        if($this->bodyText) {
            $rest->setParameter('Message.Body.Text.Data', $this->bodyText);
            $rest->setParameter('Message.Body.Text.Charset', $this->bodyCharset);
        }

        if($this->bodyHtml) {
            $rest->setParameter('Message.Body.Html.Data', $this->bodyHtml);
            $rest->setParameter('Message.Body.Html.Charset', $this->bodyCharset);
        }
        return $rest;
    }
}
