<?php
/**
*
* Copyright (c) 2011, Dan Myers.
* Parts copyright (c) 2008, Donovan Schonknecht.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* - Redistributions of source code must retain the above copyright notice,
*   this list of conditions and the following disclaimer.
* - Redistributions in binary form must reproduce the above copyright
*   notice, this list of conditions and the following disclaimer in the
*   documentation and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
* This is a modified BSD license (the third clause has been removed).
* The BSD license may be found here:
* http://www.opensource.org/licenses/bsd-license.php
*
* Amazon Simple Email Service is a trademark of Amazon.com, Inc. or its affiliates.
*
* SimpleEmailService is based on Donovan Schonknecht's Amazon S3 PHP class, found here:
* http://undesigned.org.za/2007/10/22/amazon-s3-php-class
*
*/

/**
* Amazon SES PHP class
*
* @link http://sourceforge.net/projects/php-aws-ses/
* version 0.8.2
*
*/

namespace Voodoo\Component\AWS;

use Exception;

class SES
{


    protected $__accessKey; // AWS Access key
    protected $__secretKey; // AWS Secret key
    protected $__host;

    public function getAccessKey() { return $this->__accessKey; }
    public function getSecretKey() { return $this->__secretKey; }
    public function getHost() { return $this->__host; }

    protected $__verifyHost = false;
    protected $__verifyPeer = false;

    // verifyHost and verifyPeer determine whether curl verifies ssl certificates.
    // It may be necessary to disable these checks on certain systems.
    // These only have an effect if SSL is enabled.
    public function verifyHost() { return $this->__verifyHost; }
    public function enableVerifyHost($enable = true) { $this->__verifyHost = $enable; }

    public function verifyPeer() { return $this->__verifyPeer; }
    public function enableVerifyPeer($enable = true) { $this->__verifyPeer = $enable; }

    /**
    * Constructor
    *
    * @param string $accessKey Access key
    * @param string $secretKey Secret key
    * @return void
    */
    public function __construct($host = 'email.us-east-1.amazonaws.com') {
            $this->setAuth(Auth::getAccessKey(), Auth::getSecretKey());
            $this->__host = $host;
    }

    /**
    * Set AWS access key and secret key
    *
    * @param string $accessKey Access key
    * @param string $secretKey Secret key
    * @return void
    */
    public function setAuth($accessKey, $secretKey) {
            $this->__accessKey = $accessKey;
            $this->__secretKey = $secretKey;
    }


    /**
    * Given a SES\Message object, submits the message to the service for sending.
    *
    * @return string the message id
    */
    public function sendEmail(SES\Message $sesMessage) {
        
        $rest = new SES\Request($this, 'POST');
        $rest->setParameter('Action', 'SendEmail');
        $sesMessage->prepareMessage($rest);
        $rest = $rest->getResponse();

        return (string)$rest->body->SendEmailResult->MessageId;
    }




    /**
    * Lists the email addresses that have been verified and can be used as the 'From' address
    *
    * @return Array lis all emails
    */
    public function listVerifiedEmailAddresses() {
        $rest = new SES\Request($this, 'GET');
        $rest->setParameter('Action', 'ListVerifiedEmailAddresses');

        $rest = $rest->getResponse();

        if(!isset($rest->body)) {
            return [];
        }

        $addresses = [];
        foreach($rest->body->ListVerifiedEmailAddressesResult->VerifiedEmailAddresses->member as $address) {
            $addresses[] = (string)$address;
        }
        return $addresses;
    }

    /**
    * Requests verification of the provided email address, so it can be used
    * as the 'From' address when sending emails through SES\.
    *
    * After submitting this request, you should receive a verification email
    * from Amazon at the specified address containing instructions to follow.
    *
    * @param string email The email address to get verified
    * @return bool
    */
    public function verifyEmailAddress($email) {
        $rest = new SES\Request($this, 'POST');
        $rest->setParameter('Action', 'VerifyEmailAddress');
        $rest->setParameter('EmailAddress', $email);

        $rest = $rest->getResponse();

        return true;
    }


    /**
    * Removes the specified email address from the list of verified addresses.
    *
    * @param string email The email address to remove
    * @return bool
    */
    public function deleteVerifiedEmailAddress($email) {
        $rest = new SES\Request($this, 'DELETE');
        $rest->setParameter('Action', 'DeleteVerifiedEmailAddress');
        $rest->setParameter('EmailAddress', $email);

        $rest = $rest->getResponse();

        return true;
    }


    /**
     * VerifyEmailIdentity
    * @param string email The email address to get verified
    * @return bool
    */
    public function verifyEmailIdentity($email) {
        $rest = new SES\Request($this, 'POST');
        $rest->setParameter('Action', 'VerifyEmailIdentity');
        $rest->setParameter('EmailAddress', $email);

        $rest = $rest->getResponse();

        return true;
    }


    /**
     * VerifyDomainIdentity
     * Returns A TXT record that must be placed in the DNS settings for the domain, in order to complete domain verification
    * @param string $domain
    * @return string
    */
    public function verifyDomainIdentity($domain) {
        $rest = new SES\Request($this, 'POST');
        $rest->setParameter('Action', 'VerifyDomainIdentity');
        $rest->setParameter('Domain', $domain);

        $rest = $rest->getResponse();

        return (string)$rest->body->VerifyDomainIdentityResult->VerificationToken;
    }


    /**
     * VerifyDomainDkim
     * Returns a set of DKIM tokens for a domain. DKIM tokens are character strings that represent your domain's identity.
     * Using these tokens, you will need to create DNS CNAME records that point to DKIM public keys hosted by Amazon SES.
     * Amazon Web Services will eventually detect that you have updated your DNS records; this detection process may take up to 72 hours.
     * Upon successful detection, Amazon SES will be able to DKIM-sign email originating from that domain.
     * http://docs.aws.amazon.com/ses/latest/APIReference/API_VerifyDomainDkim.html
    * @param string $domain
    * @return string
    */
    public function verifyDomainDkim($domain) {
        $rest = new SES\Request($this, 'POST');
        $rest->setParameter('Action', 'VerifyDomainDkim');
        $rest->setParameter('Domain', $domain);

        $rest = $rest->getResponse();

        $response = [];
        foreach($rest->body->VerifyDomainDkimResult->DkimTokens as $token) {
            $response[] = $token->member;
        }
        return $response;
    }

    /**
     * SetIdentityDkimEnabled
    * @param string email The email address to get verified
    * @return bool
    */
    public function setIdentityDkimEnabled($email, $enable = true) {
        $rest = new SES\Request($this, 'POST');
        $rest->setParameter('Action', 'SetIdentityDkimEnabled');
        $rest->setParameter("DkimEnabled", $enable ? "true" : "false");
        $rest->setParameter('Identity', $email);

        $rest = $rest->getResponse();

        return true;
    }


    /**
    * Retrieves information on the current activity limits for this account.
    * See http://docs.amazonwebservices.com/ses/latest/APIReference/API_GetSendQuota.html
    *
    * @return Array containing information on this account's activity limits.
    */
    public function getSendQuota() {
        $rest = new SES\Request($this, 'GET');
        $rest->setParameter('Action', 'GetSendQuota');

        $rest = $rest->getResponse();

        if(!isset($rest->body)) {
            return [];
        }

        $response = [];
        $response['Max24HourSend'] = (string)$rest->body->GetSendQuotaResult->Max24HourSend;
        $response['MaxSendRate'] = (string)$rest->body->GetSendQuotaResult->MaxSendRate;
        $response['SentLast24Hours'] = (string)$rest->body->GetSendQuotaResult->SentLast24Hours;
        $response['RequestId'] = (string)$rest->body->ResponseMetadata->RequestId;

        return $response;
    }

    /**
    * Retrieves statistics for the last two weeks of activity on this account.
    * See http://docs.amazonwebservices.com/ses/latest/APIReference/API_GetSendStatistics.html
    *
    * @return Array of activity statistics.  Each array item covers a 15-minute period.
    */
    public function getSendStatistics() {
        $rest = new SES\Request($this, 'GET');
        $rest->setParameter('Action', 'GetSendStatistics');

        $rest = $rest->getResponse();

        if(!isset($rest->body)) {
                return [];
        }

        $datapoints = array();
        foreach($rest->body->GetSendStatisticsResult->SendDataPoints->member as $datapoint) {
            $datapoints[] = [
                'Bounces' => (string)$datapoint->Bounces,
                'Complaints' => (string)$datapoint->Complaints,
                'DeliveryAttempts' => (string)$datapoint->DeliveryAttempts,
                'Rejects' => (string)$datapoint->Rejects,
                'Timestamp' => (string)$datapoint->Timestamp
            ];
        }
        return $datapoints;
    }




}



